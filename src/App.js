/*
	Main app container, also where the cookie state lives
*/

import React, { useEffect } from 'react';
import { createGlobalStyle } from 'styled-components'

import useInterval from './hooks/useInterval';
import { saveState } from './state/storage';
import { useCookies, CookieContext } from './state/cookies';

import Cookie from './components/Cookie';
import CookieCounter from './components/CookieCounter';
import ItemShop from './components/ItemShop';
import Bottom from './styles/Bottom';
import ItemDisplay from './components/ItemDisplay';

import { ITEMS } from './items';

const GlobalStyle = createGlobalStyle`
	body {
		overflow: hidden;
		margin: 0px;
		font-family: 'Montserrat', sans-serif;
	}
`;

const App = () => {
	const [state, dispatch] = useCookies();
	useEffect(() => {
		document.title = `${state.cookies} cookies`;
	});

	// Apply item effects, save state, every second
	useInterval(() => {
		ITEMS.forEach(item => {
			// Guard for newly added items which player does not have in state
			const itemCount = state.items[item.name] || 0;
			dispatch({ action: 'addCookie', amount: itemCount * item.cookiesGenerated });
		});

		saveState(state);
	}, 500);

	return (
		<CookieContext.Provider value={{ state, dispatch }}>
			<GlobalStyle />
			<CookieCounter />
			<Cookie />
			{ITEMS.map(item => state.items[item.name] > 0 && <ItemDisplay item={item} count={state.items[item.name]} />)}
			<Bottom>
				<ItemShop />
			</Bottom>
		</CookieContext.Provider>
	);
};

export default App;
