/*
	Renders items the player has purchased
*/

import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';

import { ITEM_SHAPE } from '../items';

const ItemContainer = styled.div`
	margin-left: 0.5em;
	margin-right: 0.5em;
`;
	
const ItemTitle = styled.span`
	font-size: 12px;
	margin-right: 0.5em;
`;

const ItemImage = styled.img`
	height: 16px;
`;

const makeItems = (item, count) => {
	const items = [];

	for (let i = 0; i < count; ++i) {
		items.push(<ItemImage
			alt={item.name}
			src={item.image}
		/>);
	}

	return items;
}
const ItemDisplay = ({ item, count }) => (
	<ItemContainer>
		<ItemTitle>{item.title}s:</ItemTitle>
		{makeItems(item, count)}

	</ItemContainer>
);

ItemDisplay.propTypes = {
	item: ITEM_SHAPE.isRequired,
	count: PropTypes.number.isRequired,
};

export default ItemDisplay;
