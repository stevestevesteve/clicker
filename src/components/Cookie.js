/*
	A big cookie you can click on
*/

import React, { useContext } from 'react';
import styled, { keyframes } from 'styled-components';

import { CookieContext } from '../state/cookies';
import Center from '../styles/Center';

import cookieImage from '../assets/cookie.png';

const ANIMATION_GROW = keyframes`
	from {
		transform: scale(1);
	}

	to {
		transform: scale(1.1);
	}
`;

const CookieButton = styled.button`
	margin: 4em;
	
	border: none;
	background-color: transparent;

	cursor: pointer;

	touch-action: manipulation;
	user-select: none;

	&:focus {
		outline: 0;
	}

	&:hover {
		animation: ${ANIMATION_GROW} 1s linear infinite alternate;
	}
`;

const Cookie = () => {
	const { dispatch } = useContext(CookieContext);

	return (
		<Center>
			<CookieButton onClick={() => dispatch({ action: 'addCookie', amount: 1 })}>
				<img alt="Cookie" src={cookieImage} />
			</CookieButton>
		</Center>
	);
};

export default Cookie;