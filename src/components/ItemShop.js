/*
	Renders a purchase button for each item type provided
*/

import React from 'react';

import { ITEMS } from '../items';
import ItemButton from './ItemButton';

const ItemShop = () => ITEMS.map(item => <ItemButton key={`buy-${item.name}`} item={item} />);

export default ItemShop;
