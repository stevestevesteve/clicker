/*
	Simply displays the number of cookies the player has
*/

import React, { useContext } from 'react';

import { CookieContext } from '../state/cookies';
import Banner from '../styles/Banner';

const CookieCounter = () => {
	const { state } = useContext(CookieContext);

	return (
		<>
			<Banner>
				{state.cookies} cookies
			</Banner>
		</>
	);
};

export default CookieCounter;