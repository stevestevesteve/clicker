/*
	Button for purchasing the provided item, if the player can afford it
*/

import React, { useContext } from 'react';
import styled, { keyframes } from 'styled-components';

import { CookieContext } from '../state/cookies';
import { getPrice } from '../items/priceHandling';
import { ITEM_SHAPE } from '../items';

const ANIMATION_GROW = keyframes`
	from {
		transform: scale(1);
	}

	to {
		transform: scale(1.05);
	}
`;

const ANIMATION_SHRINK = keyframes`
	from {
		transform: scale(1.05);
	}

	to {
		transform: scale(1);
	}
`;

const BuyItemButton = styled.button`
	margin: 0.5em;
	
	border: 1px solid black;
	background-color: ${props => props.disabled ? '#555' : '#none' };
	height: 48px;

	cursor: ${props => props.disabled ? 'inherit' : 'pointer' };

	&:focus {
		outline: 0;
	}
`;

const ItemImage = styled.img`
	opacity: ${props => props.disabled ? '0.2' : '1' };
	height: 32px;

	animation: ${ANIMATION_SHRINK} 0.2s linear 1 normal forwards;
	&:hover {
		animation: ${ANIMATION_GROW} 0.2s linear 1 normal forwards;
	}
`;

const ItemButton = ({ item }) => {
	const { state, dispatch } = useContext(CookieContext);

	const count = state.items[item.name] || 0;

	const price = getPrice(item, count, state.cookies);
	const canBuy = state.cookies >= price;

	return (
		<BuyItemButton disabled={!canBuy} onClick={() => dispatch({ action: 'buy', item: item.name, price })}>
			<ItemImage alt={item.title} title={item.title} src={item.image} disabled={!canBuy} />
			<span>{price}</span>
		</BuyItemButton>
	);
};

ItemButton.propTypes = {
	item: ITEM_SHAPE.isRequired,
};


export default ItemButton;