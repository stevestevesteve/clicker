/*
	For now, use localStorage to save cookie state
*/

const LOCAL_STORAGE_KEY = 'steveCookies';
const INITIAL_STATE = {
	cookies: 0,
	items: {
		cursor: 0,
	}
};

export const saveState = (state) => {
	try {
		localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state));
	} catch(e) {
		console.log('Failed to save state', e);
	}
};

export const loadState = () => {
	try {
		// Store data is not validated, but probably should be in
		// a real app
		const state = localStorage.getItem(LOCAL_STORAGE_KEY);
		if (state) {
			return JSON.parse(state);
		}
	} catch(e) {
		console.log('Failed to load state', e);
	}

	return INITIAL_STATE;
}