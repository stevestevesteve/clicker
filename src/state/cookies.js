/*
	Handles cookie clicking and purchasing state mutation
*/

import React, { useReducer } from 'react';

import { loadState } from './storage';

const cookieReducer = (state, data) => {
	const { action } = data;

	switch (action) {
		case 'addCookie':
			const cookiesToAdd = data.amount;
			return {
				...state,
				cookies: state.cookies + cookiesToAdd
			};
		case 'buy':
			const { item, price } = data;
			if (price > state.cookies) {
				// can't afford it
				return state;
			}

			const currentItemCount = state.items[item] || 0;
			return {
				...state,
				cookies: state.cookies - price,
				items: {
					...state.items,
					[item]: currentItemCount + 1,
				},
			};
		default:
			console.log(`Unknown action '${action}' dispatched`);
			return state;
		}
};

// Simple hook which provides the reducer and loads state from storage
export const useCookies = () => useReducer(cookieReducer, loadState());

// Basic context for providing state, reducer to components
export const CookieContext = React.createContext(null);
