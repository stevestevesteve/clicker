// try and increase the price by some exponential curve,
// I'm tweaking item.priceIncrease and checking the result
//
// this is not really my area
export const getPrice = (item, count) =>
	Math.floor(item.baseCost * Math.pow(1 + item.priceIncrease, count));
