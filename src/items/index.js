/*
	Defines all available items in an easy-to-import array,
	plus a propTypes shape for the item definition
*/

import PropTypes from 'react-proptypes';

import CURSOR from './cursor';
import WARP from './warp';

export const ITEMS = [
	CURSOR,
	WARP,
];

export const ITEM_SHAPE = PropTypes.shape({
	name: PropTypes.string,
	title: PropTypes.string,
	image: PropTypes.string,

	baseCost: PropTypes.number,
	priceIncrease: PropTypes.number,
	cookiesGenerated: PropTypes.number,
});
