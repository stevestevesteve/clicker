import warp from '../assets/items/warp.png';

const WARP = {
	name: 'warp',
	title: 'Time-space Cookie Warp',
	image: warp,

	baseCost: 200,
	priceIncrease: 0.09,
	cookiesGenerated: 10,
};

export default WARP;
