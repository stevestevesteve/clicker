/*
	Item definition for cursors, any new items introduced
	would then follow this same template
*/

import cursor from '../assets/items/cursor.png';

const CURSOR = {
	name: 'cursor',
	title: 'Cursor',
	image: cursor,

	baseCost: 10,
	priceIncrease: 0.08,
	cookiesGenerated: 1,
};

export default CURSOR;
