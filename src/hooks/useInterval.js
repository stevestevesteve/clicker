/*
	Instead of reinventing the wheel, I've used the setInterval hook
	described here: https://overreacted.io/making-setinterval-declarative-with-react-hooks/
	with a couple of minor style changes
*/

import { useEffect, useRef } from 'react';

const useInterval = (callback, delay) => {
	const savedCallback = useRef();

	// Remember the latest callback.
	useEffect(() => {
		savedCallback.current = callback;
	}, [callback]);

	// Set up the interval.
	useEffect(() => {
		function tick() {
			savedCallback.current();
		}
		if (delay !== null) {
			let id = setInterval(tick, delay);
			return () => clearInterval(id);
		}
	}, [delay]);
};

export default useInterval;
