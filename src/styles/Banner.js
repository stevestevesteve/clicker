/*
	A simple full-width banner with provided or default background color
*/

import PropTypes from 'react-proptypes';
import styled from 'styled-components';

const PADDING = '1em';
const DEFAULT_COLOR = '#19386b';

const Banner = styled.div`
	background-color: ${props => props.backgroundColor || DEFAULT_COLOR};
	color: #fff;

	text-align: center;
	font-size: 40px;
	
	padding: ${PADDING};
`;

Banner.propTypes = {
	backgroundColor: PropTypes.string,
};

export default Banner;
