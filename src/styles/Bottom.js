/*
	Declaratively attach stuff to the bottom of the page
*/

import styled from 'styled-components';

const Bottom = styled.div`
	position: absolute;
	bottom: 0;
`;

export default Bottom;